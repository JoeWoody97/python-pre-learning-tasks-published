def vowel_swapper(string):
    # ==============

    new_string = ""
    character_counts = {}

    for char in string:
        count = character_counts.get(char.lower(), 0)
        replacement = char
        if count == 1:
            if (char == "a") or (char == "A"):
                replacement = "4"
            elif (char == "e") or (char == "E"):
                replacement = "3"
            elif (char == "i") or (char == "I"):
                replacement = "!"
            elif char == "o":
                replacement = "ooo"
            elif (char == "u") or (char == "U"):
                replacement = "|_|"
            elif char == "O":
                replacement = "000"

        new_string = new_string + replacement
        character_counts[char.lower()] = count + 1

    return new_string

    # ==============


print(vowel_swapper("aAa eEe iIi oOo uUu"))  # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World"))  # Should print "Hello Wooorld" to the console
print(vowel_swapper("Everything's Available"))  # Should print "Ev3rything's Av/\!lable" to the console
